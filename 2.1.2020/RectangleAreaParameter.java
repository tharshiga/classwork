public class RectangleAreaParameter{
  public static void main(String args[]){
    double l = 5, w = 4;
    double a = l * w;
    double p = 2 * (l + w);
    System.out.println("area = " + a);
    System.out.println("Parameter = " + p);
  }
}
