public class TriangleAreaParameter{
  public static void main(String args[]){
    double w = 40, h = 30;
    double a = 3 * w;
    double p = 0.5 * w * h;
    System.out.println("area = " + a);
    System.out.println("Parameter = " + p);
  }
}
